TARGET = $(notdir $(CURDIR))
OBJS = main.o i2c.o i2c_oled.o

CC = xtensa-lx106-elf-gcc
CFLAGS = -I. -mlongcalls
LDLIBS = -nostdlib -Wl,--start-group -lmain -lnet80211 -lwpa -llwip -lpp -lphy -Wl,--end-group -lgcc
LDFLAGS = -Teagle.app.v6.ld
BAUDRATE = 921600

all: $(TARGET)-0x00000.bin

$(TARGET)-0x00000.bin: $(TARGET).elf
	esptool.py elf2image $^ -o "$(basename $^)-"

$(TARGET).elf: $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

flash: $(TARGET)-0x00000.bin
	esptool.py -b $(BAUDRATE) write_flash 0 $(TARGET)-0x00000.bin 0x40000 $(TARGET)-0x40000.bin

clean:
	rm -f $(TARGET).elf $(OBJS) $(TARGET)-0x00000.bin $(TARGET)-0x40000.bin
